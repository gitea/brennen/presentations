#!/usr/bin/env perl

use warnings;
use strict;
use 5.10.0;

say greet($ARGV[0]);

sub greet {
  my ($greetee) = @_;
  return "Hello $greetee.";
}
