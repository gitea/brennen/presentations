#!/usr/bin/env perl

use warnings;
use strict;
use 5.10.0;

while (<>) {
  # Extract name where given name matches "John":
  say "$2 $1" if m/^(.*)\t(Jo.*?)(\t|$)/i;
}
