#!/usr/bin/env perl

use warnings;
use strict;
use 5.10.0;

# Quote a pattern to match:
#   - /x means "extended syntax" - can include whitespace and comments
#   - /i means "case insensitive"
#   - qr is a quote operator - quotes everything between the delimiters
#     as a regular expression
my $pattern = qr/
  ^                       # Start
    ( \d{4}-\d{2}-\d{2} ) # Date
    .*                    # Everything else
  $                       # End of line
/xi;

# Loop over each line of:
#   1. standard input, if given
#   OR
#   2. the files given on the CLI
while (<>) {
  # The line is now in $_, the default variable.

  # Print the first capture group if we got a match:
  say $1 if m/$pattern/;
}
