# presentations

This started as rough slides / notes for a couple of sessions at the Spring
2020 Wikimedia Engineering Productivity offsite.  It may expand to include
other things.

## contents

* [All the Perl you need for slicing and dicing text files](perl/)
  ( [slides](https://squiggle.city/~brennen/perl/) )
* [Tricking out your Vim to a moderately ridiculous degree](vim/)
