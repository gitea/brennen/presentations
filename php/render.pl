#!/usr/bin/env perl

use strict;
use warnings;
use 5.10.0;

use Cwd;
use File::Basename;
use Text::Markdown::Discount;
use IPC::System::Simple qw(capturex);

# Enable html5 block-level tags:
Text::Markdown::Discount::with_html5_tags();
my $flags = Text::Markdown::Discount::MKD_EXTRA_FOOTNOTE();
my $markdown = Text::Markdown::Discount->new;

my $cwd = Cwd::getcwd();

my $full_source = '';
while (my $source = get_input()) {

  # A simple preprocessor:
  my ($basename, $dir) = fileparse($ARGV); # get path of target file
  chdir $dir;
  $source =~ s{```php(.*?)```}{handle_block($1);}egs;
  chdir $cwd;

  $full_source .= $source;

}

print $full_source;
# actually spit out the contents of the slide

sub get_input {
  local $/ = undef;
  my $source = <>;
  return $source;
}

sub handle_block {
  my ($block) = @_;

  my $cmd;

  file_put_contents('/tmp/phpslideoutput.php', $block);
  my $result = `php /tmp/phpslideoutput.php`;
  chomp($result);

  if ($result) {
    return "```php${block}```\n\nOutput:\n\n```${result}\n```";
  } else {
    return "```php${block}```";
  }

}

sub file_put_contents {
  my ($file, $contents) = @_;
  open(my $fh, '>', $file)
    or die "Unable to open $file for writing: $!";
  print $fh $contents;
  close $fh;
}
